# Checklist APP

An Ionic-App to maintain checklists and easily enter values in them, once per day. The App features different roles for User and Admin and allows the Admin to view all entries in a custom calendar view. Existing entires are immutable, but they can be deleted or transferred via email.

### Prerequisites

What things you need to install the software and how to install them

to run:

* nodejs (tested with v18.9.0)

to build & deploy follow [Ionic v1 Installation](https://ionicframework.com/docs/v1/guide/installation.html):

* OpenJDK 1.8 (aka "8") with required path variables (JAVA_HOME).
  I recommend installing it from [here](https://github.com/ojdkbuild/ojdkbuild) to avoid [this error](https://stackoverflow.com/questions/68743003/trustanchors-parameter-must-be-non-empty-error-gradle-cordova).
* Android Studio with required path variables (ANDROID_HOME, tools, platform-tools)
* [Gradle](https://gradle.org/install/) (required by newer versions of Cordova)
* Ant is **not** necessary (even though it is mentioned in the Ionic Documentation)

### Installing

* run `npm install` (first time only)
* run `npm start`

See deployment for notes on how to deploy the project on a live system.

## Deployment

follow [this process](https://ionicframework.com/docs/v1/guide/publishing.html) to publish:

* create a release apk: `npm run build`
* first time only, key is already checked into repository: `keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000`, use keygen password: `checklist`.
* sign the app: `jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk alias_name`. You will be prompted to enter the password for the release key again.
%ANDROID_HOME%
* optimize the apk: `C:\Users\Micha\AppData\Local\Android\Sdk\build-tools\31.0.0\zipalign -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk checklist.apk` with the correct path to your build-tools.
* setup on tablets with password: "Autohof1233"

## Versioning

## Authors

* **Micha Sengotta** - *Initial work* - [michasng](https://gitlab.com/michasng)

See also the list of [contributors](https://gitlab.com/michasng/checklistapp/graphs/master) who participated in this project.

## License

This project is licensed under the a private License - see the [LICENSE.md](LICENSE.md) file for details
TODO: add license.md, for now please contact the owner (Micha Sengotta)
