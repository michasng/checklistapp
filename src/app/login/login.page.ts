import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from '../api/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  password: string;
  passwordInvalid: boolean;
  userExists: boolean = null;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private authService: AuthService
  ) {}

  navSub: Subscription;

  async ngOnInit() {
    this.navSub = this.router.events.subscribe(async event => {
      if (event instanceof NavigationEnd && /\/login/.test(event.url))
        await this.init();
    });
    await this.init();
  }

  async ngOnDestroy() {
    this.navSub.unsubscribe();
  }

  async init() {
    this.password = '';
    this.passwordInvalid = false;

    this.userExists = await this.authService.userExists();
  }

  async setAdminPassword() {
    await this.authService.setPassword(this.password);
    this.password = '';
    this.userExists = true;
  }

  passwordInput() {
    this.passwordInvalid = false;
  }

  selectEmployee() {
    this.authService.logout();
    this.goToTodaysChecklist();
  }

  async selectAdmin() {
    console.log('password', this.password);
    if (await this.authService.login(this.password)) {
      this.passwordInvalid = false;
      this.goToListOverview();
    } else {
      this.passwordInvalid = true;
      const toast = await this.toastCtrl.create({
        message: 'Ungültiges Passwort',
        duration: 2000,
      });
      toast.present();
    }
  }

  goToTodaysChecklist() {
    this.router.navigate(['list-choice']);
  }

  goToListOverview() {
    this.router.navigate(['list-overview']);
  }
}
