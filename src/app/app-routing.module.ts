import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppAuthGuard } from './app-auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {
    path: 'list-choice',
    loadChildren: './list-choice/list-choice.module#ListChoicePageModule',
  },
  {
    path: 'checklist/:year/:month/:day/:listTypeId',
    loadChildren: './checklist/checklist.module#ChecklistPageModule',
  },
  {
    path: 'list-overview',
    loadChildren: './list-overview/list-overview.module#ListOverviewPageModule',
    canActivate: [AppAuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
