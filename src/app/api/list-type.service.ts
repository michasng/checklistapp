import { Injectable } from '@angular/core';

export interface ListType {
  name: string;
  filename: string;
  shortname: string;
}

@Injectable({
  providedIn: 'root',
})
export class ListTypeService {
  private _listTypes: ListType[];
  get listTypes(): ListType[] {
    return this._listTypes;
  }

  constructor() {
    this.init();
  }

  init() {
    this._listTypes = [];
    this._listTypes.push({
      name: 'Grundreinigung nach Arbeitsende: Koch/in',
      filename: 'list_cleaningKitchenChef',
      shortname: 'Koch',
    } as ListType);
    this._listTypes.push({
      name: 'Arbeitsplatz Küchenhilfe',
      filename: 'list_cleaningKitchenStaff',
      shortname: 'Küchenhilfe',
    } as ListType);
    this._listTypes.push({
      name: 'Reinigungsplan Bistro Restaurant',
      filename: 'list_cleaningBistroRestaurant',
      shortname: 'Bistro',
    } as ListType);
    this._listTypes.push({
      name: 'Reinungsplan Kundenbereich',
      filename: 'list_cleaningCustomerArea',
      shortname: 'Kundenbereich',
    } as ListType);
  }

  public getId(listType: ListType): number {
    return this.listTypes.indexOf(listType);
  }

  public getListType(id: number): ListType {
    return this.listTypes[id];
  }
}
