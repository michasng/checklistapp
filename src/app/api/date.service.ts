import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateService {
  constructor() {}

  // Note:
  // datestring has day-accuracy
  // timestamp has minute-accuracy

  paramsToDatestring(year: string, month: string, day: string): string {
    const date = new Date(Number(year), Number(month) - 1, Number(day));
    const timestamp = this.getTimestamp(date);
    const datestring = this.timestampToString(timestamp);
    return datestring;
  }

  timestampToString(timestamp: number): string {
    const date = new Date(timestamp * 1000);
    return (
      date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
    );
  }

  dateFromString(datestring: string): Date {
    const segments = datestring.split('.');
    if (segments.length !== 3) {
      console.error('Invalid datestring', datestring);
      return new Date();
    }
    const date = new Date(0);
    date.setDate(Number(segments[0]));
    date.setMonth(Number(segments[1]) - 1);
    date.setFullYear(Number(segments[2]));
    return date;
  }

  timestampFromString(datestring: string): number {
    return this.getTimestamp(this.dateFromString(datestring));
  }

  getCurrentTimestamp(): number {
    return this.getTimestamp(new Date());
  }

  isToday(timestamp: number): boolean {
    return (
      this.timestampToString(timestamp) ===
      this.timestampToString(this.getCurrentTimestamp())
    );
  }

  getTimestamp(date: Date): number {
    return Math.floor(date.getTime() / 1000);
  }
}
