import { Injectable } from '@angular/core';
import { ChecklistService } from './checklist.service';
import { DateService } from './date.service';
import { ListTypeService } from './list-type.service';

@Injectable({
  providedIn: 'root',
})
export class MockService {
  constructor(
    private checklistService: ChecklistService,
    private listTypeService: ListTypeService,
    private dateService: DateService
  ) {}

  async generateRandomMocks(amount: number) {
    for (let i = 0; i < amount; i++) await this.generateRandomMock();
  }

  async generateRandomMock() {
    const numListTypes = this.listTypeService.listTypes.length;
    const listTypeId = Math.floor(Math.random() * numListTypes);
    const oneYearAgo = new Date();
    oneYearAgo.setFullYear(oneYearAgo.getFullYear() - 1);
    const upperLimit = this.dateService.getCurrentTimestamp();
    const lowerLimit = this.dateService.getTimestamp(oneYearAgo);
    const timestamp = lowerLimit + Math.random() * (upperLimit - lowerLimit);
    const checklist = await this.checklistService.createDefaultChecklist(
      timestamp,
      listTypeId
    );
    checklist.employee = Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, '');
    for (const section of checklist.sections)
      for (const item of section.items)
        item.value = Boolean(Math.floor(Math.random() * 2));
    await this.checklistService.setChecklist(checklist, listTypeId);
  }
}
