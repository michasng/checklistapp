import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

export interface UniqueChecklist {
  datestring: string;
  listTypeId: number;
}

@Injectable({
  providedIn: 'root',
})
export class ChecklistsService {
  constructor(private storage: Storage) {}

  async init(): Promise<any> {
    console.log('init checklists');
    const checklistsStr = await this.storage.get('checklists');
    if (checklistsStr === null)
      return await this.storage.set('checklists', JSON.stringify([]));
  }

  async getChecklists(): Promise<Array<UniqueChecklist>> {
    return JSON.parse(await this.storage.get('checklists')) as Array<
      UniqueChecklist
    >;
  }

  async setChecklists(checklists: Array<UniqueChecklist>): Promise<any> {
    return await this.storage.set('checklists', JSON.stringify(checklists));
  }

  async addUniqueList(datestring: string, listTypeId: number): Promise<any> {
    const checklists = await this.getChecklists();
    checklists.push({ datestring, listTypeId } as UniqueChecklist);
    return await this.setChecklists(checklists);
  }

  async removeUniqueList(datestring: string, listTypeId: number): Promise<any> {
    const checklists = await this.getChecklists();
    let index: number;
    for (let i = 0; i < checklists.length; i++)
      if (
        checklists[i].datestring === datestring &&
        checklists[i].listTypeId === listTypeId
      )
        index = i;
    checklists.splice(index, 1);
    await this.setChecklists(checklists);
  }

  async checklistExists(
    datestring: string,
    listTypeId: number
  ): Promise<boolean> {
    const checklists = await this.getChecklists();
    for (const uniqueList of checklists)
      if (
        uniqueList.datestring === datestring &&
        uniqueList.listTypeId === listTypeId
      )
        return true;
    return false;
  }

  async anyChecklistExists(datestring: string): Promise<boolean> {
    const checklists = await this.getChecklists();
    for (const uniqueList of checklists)
      if (uniqueList.datestring === datestring) return true;
    return false;
  }
}
