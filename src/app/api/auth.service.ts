import { Injectable, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements OnInit {
  private loggedIn: boolean;

  constructor(private storage: Storage) {}

  ngOnInit(): void {
    this.loggedIn = false;
  }

  async setPassword(password: string): Promise<any> {
    return await this.storage.set('password', password);
  }

  async userExists(): Promise<boolean> {
    return (await this.storage.get('password')) !== null;
  }

  async login(password: string): Promise<boolean> {
    this.loggedIn = (await this.storage.get('password')) === password;
    return this.loggedIn;
  }

  logout() {
    this.loggedIn = false;
  }

  isLoggedIn(): boolean {
    return this.loggedIn;
  }
}
