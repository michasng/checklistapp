import { Injectable } from '@angular/core';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ToastController } from '@ionic/angular';
import * as xlsx from 'xlsx';
import { ChecklistService } from './checklist.service';
import { ListTypeService } from './list-type.service';

@Injectable({
  providedIn: 'root',
})
export class ExportService {
  constructor(
    private checklistService: ChecklistService,
    private listTypeService: ListTypeService,
    private file: File,
    private emailComposer: EmailComposer,
    private toastCtrl: ToastController
  ) {}

  async export(mailAddress: string): Promise<boolean> {
    const checklists = [];
    for (const listType of this.listTypeService.listTypes) {
      const listTypeId = this.listTypeService.getId(listType);
      const table = await this.checklistService.getChecklistTable(listTypeId);
      if (table === null) continue;
      checklists[listTypeId] = table;
    }

    const SheetNames = [];
    const Sheets = {};
    for (const listType of this.listTypeService.listTypes) {
      const listTypeId = this.listTypeService.getId(listType);
      if (checklists[listTypeId] === undefined) continue;
      SheetNames.push(listType.shortname);
      // console.log('converting', checklists[listTypeId]);
      Sheets[listType.shortname] = xlsx.utils.aoa_to_sheet(
        checklists[listTypeId]
      );
      // console.log('sheet', Sheets[listType.shortname]);
      Sheets[listType.shortname]['!cols'] = [
        {
          width: 70,
        },
      ];
    }
    const workbook = { SheetNames, Sheets };
    // console.log('workbook', workbook);

    const blob = this.createBlobFromWorkbook(workbook);
    const directory = this.file.externalDataDirectory;
    const filename = 'export.xlsx';
    const writeRes = await this.writeFile(blob, directory, filename);
    if (writeRes === false) return false;

    const filepath = directory + filename;
    if (mailAddress === '') {
      const toast = await this.toastCtrl.create({
        message: 'Dateiexport wurde ohne Email ausgeführt.',
        duration: 2000,
      });
      toast.present();
    }
    return await this.sendEmail(mailAddress, filepath);
  }

  createBlobFromWorkbook(workbook) {
    let wbout = xlsx.write(workbook, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary',
    });

    const s2ab = s => {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
      return buf;
    };

    return new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
  }

  async writeFile(
    content: string | Blob,
    directory: string,
    filename: string
  ): Promise<boolean> {
    console.log('writing file to', directory, filename);
    try {
      await this.file.writeFile(directory, filename, content, {
        append: false,
        replace: true,
      });
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  async sendEmail(mailAddress: string, filepath: string): Promise<any> {
    // this.emailComposer.isAvailable().then((available: boolean) => {
    //   if (available) {
    //     //Now we know we can send
    //   }
    // });

    const email = {
      to: mailAddress,
      // cc: 'erika@mustermann.de',
      // bcc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        filepath,
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf',
      ],
      subject: 'Checkliste Tabellen-Export',
      body:
        'Den Tabellen-Export können Sie dem Anhang entnehmen.\nDies ist ein Service der Checklist-App.',
      // isHtml: true,
    };
    console.log('sending mail', email);

    // Send a text message using default options
    return await this.emailComposer.open(email);
  }
}
