import { Pipe, PipeTransform } from '@angular/core';
import { ChecklistsService } from './checklists.service';
import { DateService } from './date.service';

@Pipe({ name: 'checklistExists', pure: true })
export class ChecklistExistsPipe implements PipeTransform {
  constructor(
    private dateService: DateService,
    private checklistsService: ChecklistsService
  ) {}

  async transform(
    value: number,
    year: number,
    month: number,
    updated: Date // used to force-update the pipe
  ): Promise<boolean> {
    const date = new Date(year, month, value);
    const timestamp = this.dateService.getTimestamp(date);
    const datestring = this.dateService.timestampToString(timestamp);

    const bool = await this.checklistsService.anyChecklistExists(datestring);
    return bool;
  }
}
