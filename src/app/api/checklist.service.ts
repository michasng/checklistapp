import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Checklist } from '../model/checklist.data';
import { ChecklistsService, UniqueChecklist } from './checklists.service';
import { ListType, ListTypeService } from './list-type.service';
import { DateService } from './date.service';

@Injectable({
  providedIn: 'root',
})
export class ChecklistService {
  constructor(
    private storage: Storage,
    private http: HttpClient,
    private listTypeService: ListTypeService,
    private dateService: DateService,
    private checklistsService: ChecklistsService
  ) {}

  async createDefaultChecklist(
    timestamp: number,
    listTypeId: number
  ): Promise<Checklist> {
    const datestring = this.dateService.timestampToString(timestamp);
    const listType = this.listTypeService.getListType(listTypeId);
    console.log('create Checklist for day', datestring, timestamp);
    let checklist: Checklist;
    try {
      checklist = (await this.http
        .get('./assets/' + listType.filename + '.json')
        .toPromise()) as Checklist;
    } catch (e) {
      console.error(e);
      checklist = new Checklist();
    }
    checklist.timestamp = timestamp;
    return checklist;
  }

  async getChecklist(
    timestamp: number,
    listTypeId: number
  ): Promise<Checklist> {
    const datestring = this.dateService.timestampToString(timestamp);
    console.log('get Checklist for day', datestring, timestamp);
    const stringItem = await this.storage.get(
      this.storageName(datestring, listTypeId)
    );
    if (stringItem === null)
      return await this.createDefaultChecklist(timestamp, listTypeId);
    try {
      const obj = JSON.parse(stringItem);
      return obj as Checklist;
    } catch (e) {
      console.error(e);
      return await this.createDefaultChecklist(timestamp, listTypeId);
    }
  }

  async setChecklist(checklist: Checklist, listTypeId: number): Promise<any> {
    const datestring = this.dateService.timestampToString(checklist.timestamp);
    console.log('set Checklist for day', datestring, checklist.timestamp);
    if (
      !(await this.checklistsService.checklistExists(datestring, listTypeId))
    ) {
      await this.checklistsService.addUniqueList(datestring, listTypeId);
    }
    return await this.storage.set(
      this.storageName(datestring, listTypeId),
      JSON.stringify(checklist)
    );
  }

  async removeChecklist(timestamp: number, choice: ListType): Promise<any> {
    const datestring = this.dateService.timestampToString(timestamp);
    const listTypeId = this.listTypeService.getId(choice);
    console.log('remove Checklist for day', datestring, timestamp);
    await this.checklistsService.removeUniqueList(datestring, listTypeId);
    return await this.storage.remove(this.storageName(datestring, listTypeId));
  }

  async removeOldChecklists(months: number): Promise<number> {
    const maxAge = new Date();
    maxAge.setMonth(maxAge.getMonth() - months);
    console.log('remove checklists older than', maxAge.toLocaleString());
    const checklists = await this.checklistsService.getChecklists();
    const toKeep: Array<UniqueChecklist> = [];
    const toDelete: Array<UniqueChecklist> = [];
    for (let uniqueList of checklists) {
      const date = this.dateService.dateFromString(uniqueList.datestring);
      console.log(date.toLocaleString());
      if (date.getTime() > maxAge.getTime()) toKeep.push(uniqueList);
      else toDelete.push(uniqueList);
    }
    console.log('remove checklists', toDelete);
    console.log('keep checklists', toKeep);
    for (let uniqueList of toDelete)
      await this.storage.remove(
        this.storageName(uniqueList.datestring, uniqueList.listTypeId)
      );
    await this.checklistsService.setChecklists(toKeep);
    return toDelete.length;
  }

  async getChecklistTable(listTypeId: number): Promise<string[][]> {
    const checklists = await this.checklistsService.getChecklists();
    const table = [];
    const defaultChecklist = await this.createDefaultChecklist(
      this.dateService.getCurrentTimestamp(),
      listTypeId
    );
    table.push(['Datum']);
    table.push(['Uhrzeit']);
    table.push(['Mitarbeiter']);
    table.push(['Kommentare']);
    for (const section of defaultChecklist.sections)
      for (const item of section.items)
        table.push([section.title + ': ' + item.title]);
    let anyListExists = false;
    for (const uniqueList of checklists) {
      if (uniqueList.listTypeId !== listTypeId) continue;
      anyListExists = true;
      const timestamp = this.dateService.timestampFromString(
        uniqueList.datestring
      );
      const checklist = await this.getChecklist(
        timestamp,
        uniqueList.listTypeId
      );
      const date = new Date(checklist.timestamp * 1000);
      table[0].push(date.toLocaleDateString());
      table[1].push(date.toLocaleTimeString());
      table[2].push(checklist.employee);
      table[3].push(checklist.comments);
      let i = 4;
      for (const section of checklist.sections)
        for (const item of section.items) table[i++].push(String(item.value));
    }
    if (!anyListExists) return null;
    return table;
  }

  private storageName(datestring: string, listTypeId: number) {
    return 'checklist-' + listTypeId + '-' + datestring;
  }
}
