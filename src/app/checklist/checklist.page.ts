import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { ChecklistService } from '../api/checklist.service';
import { ChecklistsService } from '../api/checklists.service';
import { DateService } from '../api/date.service';
import { Checklist } from '../model/checklist.data';
import { ToastOptions } from '@ionic/core';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.page.html',
  styleUrls: ['./checklist.page.scss'],
})
export class ChecklistPage implements OnInit {
  checklist: Checklist;
  editable: boolean;
  fixedTimestamp: number;
  listTypeId: number;

  constructor(
    private dateService: DateService,
    private checklistService: ChecklistService,
    private checklistsService: ChecklistsService,
    private alertController: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private toastCtrl: ToastController
  ) {}

  async ngOnInit() {
    let date: Date;
    const year = this.route.snapshot.paramMap.get('year');
    const month = this.route.snapshot.paramMap.get('month');
    const day = this.route.snapshot.paramMap.get('day');
    this.listTypeId = Number(this.route.snapshot.paramMap.get('listTypeId'));
    console.log('day month year', day, month, year);
    console.log('listType', this.listTypeId);
    date = new Date(Number(year), Number(month) - 1, Number(day));
    console.log('date', date.toLocaleDateString());
    const timestamp = this.dateService.getTimestamp(date);
    if (this.dateService.isToday(timestamp)) this.fixedTimestamp = -1;
    else this.fixedTimestamp = timestamp;
    const datestring = this.dateService.timestampToString(timestamp);
    const exists = await this.checklistsService.checklistExists(
      datestring,
      this.listTypeId
    );
    this.editable = !exists;
    this.checklist = await this.checklistService.getChecklist(
      timestamp,
      this.listTypeId
    );
    const editable = this.route.snapshot.paramMap.get('editable');
    if (editable == 'true') this.editable = true;
  }

  async accept() {
    console.log('accepting checklist', this.checklist);
    this.editable = false;
    if (this.fixedTimestamp < 0)
      this.checklist.timestamp = this.dateService.getCurrentTimestamp();
    console.log(new Date(this.checklist.timestamp * 1000).toLocaleString());
    await this.checklistService.setChecklist(this.checklist, this.listTypeId);
    await this.router.navigate(['login']);
    const toast = await this.toastCtrl.create({
      message: 'Liste gespeichert.',
      duration: 2000,
    });
    toast.present();
  }

  async presentAcceptAlert() {
    const alert = await this.alertController.create({
      header: 'Bestätigen und Sperren?',
      message:
        'Die heutige Liste kann danach <strong>nicht</strong> mehr verändert werden!',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {}, // does nothing
        },
        {
          text: 'Bestätigen',
          handler: () => {
            this.accept();
          },
        },
      ],
    });

    await alert.present();
  }
}
