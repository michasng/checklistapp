import { ChecklistItem } from './checklist-item.data';

export class ChecklistSection {
  public title: string;
  public description: string;
  public items: ChecklistItem[];
}
