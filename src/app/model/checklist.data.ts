import { ChecklistSection } from './checklist-section.data';

export class Checklist {
  public timestamp: number;
  public employee: string;
  public listTypeId: number;
  public sections: ChecklistSection[];
  public comments: string;
}
