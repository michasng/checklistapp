import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ChecklistsService } from '../api/checklists.service';
import { DateService } from '../api/date.service';
import { ListType, ListTypeService } from '../api/list-type.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-choice',
  templateUrl: './list-choice.page.html',
  styleUrls: ['./list-choice.page.scss'],
})
export class ListChoicePage implements OnInit, OnDestroy {
  year: string;
  month: string;
  day: string;
  isToday: boolean;

  listTypeExists: boolean[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dateService: DateService,
    private listTypeService: ListTypeService,
    private checklistsService: ChecklistsService
  ) {}

  navSub: Subscription;

  async ngOnInit() {
    this.navSub = this.router.events.subscribe(async event => {
      if (event instanceof NavigationEnd && /\/list-choice/.test(event.url))
        await this.init();
    });
    await this.init();
  }

  async ngOnDestroy() {
    this.navSub.unsubscribe();
  }

  async init() {
    this.year = this.route.snapshot.paramMap.get('year');
    this.month = this.route.snapshot.paramMap.get('month');
    this.day = this.route.snapshot.paramMap.get('day');
    if (this.year === null || this.month === null || this.day === null) {
      const date = new Date();
      this.year = String(date.getFullYear());
      this.month = String(date.getMonth() + 1);
      this.day = String(date.getDate());
    }

    const datestring = this.dateService.paramsToDatestring(
      this.year,
      this.month,
      this.day
    );
    this.isToday = this.dateService.isToday(
      this.dateService.timestampFromString(datestring)
    );
    this.listTypeExists = [];
    for (const listType of this.listTypeService.listTypes) {
      this.listTypeExists.push(
        await this.checklistsService.checklistExists(
          datestring,
          this.listTypeService.getId(listType)
        )
      );
    }
  }

  async choose(listType: ListType) {
    const listTypeId = this.listTypeService.getId(listType);
    const datestring = this.dateService.paramsToDatestring(
      this.year,
      this.month,
      this.day
    );
    if (
      (await this.checklistsService.checklistExists(datestring, listTypeId)) ||
      this.isToday
    ) {
      this.router.navigate([
        'checklist',
        this.year,
        this.month,
        this.day,
        this.listTypeService.getId(listType),
      ]);
    }
  }
}
