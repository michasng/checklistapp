import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListChoicePage } from './list-choice.page';

describe('ListChoicePage', () => {
  let component: ListChoicePage;
  let fixture: ComponentFixture<ListChoicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListChoicePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListChoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
