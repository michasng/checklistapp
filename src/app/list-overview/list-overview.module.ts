import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListOverviewPage } from './list-overview.page';
import { CommonUiModule } from '../common-ui/common-ui.module';
import { ChecklistExistsPipe } from '../api/checklist-exists.pipe';

const routes: Routes = [
  {
    path: '',
    component: ListOverviewPage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CommonUiModule,
  ],
  declarations: [ListOverviewPage, ChecklistExistsPipe],
})
export class ListOverviewPageModule {}
