import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  AlertController,
  PickerController,
  ToastController,
} from '@ionic/angular';
import { Subscription } from 'rxjs';
import { ChecklistService } from '../api/checklist.service';
import { ExportService } from '../api/export.service';
import { MockService } from '../api/mock.service';
import { CalendarComponent } from '../common-ui/calendar/calendar.component';

@Component({
  selector: 'app-list-overview',
  templateUrl: './list-overview.page.html',
  styleUrls: ['./list-overview.page.scss'],
})
export class ListOverviewPage implements OnInit, OnDestroy {
  @ViewChild('calendar') calendar: CalendarComponent;
  updated: Date; // hacky way to force an update on a pure pipe

  private clickSub: Subscription;

  constructor(
    private router: Router,
    private checklistService: ChecklistService,
    private exportService: ExportService,
    private pickerCtrl: PickerController,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private mockService: MockService
  ) {}

  ngOnInit() {
    this.clickSub = this.calendar.clickSubject.subscribe(day =>
      this.dayClicked(day)
    );
  }

  ngOnDestroy(): void {
    this.clickSub.unsubscribe();
  }

  async clearStorage() {
    const options = [
      {
        text: 'alle Daten',
        value: 0,
      },
      {
        text: 'älter als 1 Monat',
        value: 1,
      },
    ];
    for (let i = 2; i < 25; i++) {
      options.push({
        text: `älter als ${i} Monate`,
        value: i,
      });
    }
    const picker = await this.pickerCtrl.create({
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
        },
        {
          text: 'Löschen',
          role: 'accept',
          // cssClass: 'ion-color ion-color-danger button', // can't set color
          handler: option => {
            console.log('clearStorage', option.months.value + ' months old');
            this.checklistService
              .removeOldChecklists(option.months.value)
              .then(async numDeleted => {
                const toast = await this.toastCtrl.create({
                  message: numDeleted + ' Einträge wurden gelöscht',
                  duration: 2000,
                });
                toast.present();
                this.updated = new Date();
              });
          },
        },
      ],
      columns: [
        {
          name: 'months',
          options,
          selectedIndex: 12,
        },
      ],
      backdropDismiss: true,
    });
    await picker.present();
  }

  async export() {
    try {
      const emailAddress = await this.presentEmailPrompt();
      console.log('emailAddress', emailAddress);
      await this.exportService.export(emailAddress);
    } catch (alertRejectError) {
      return; // do nothing
    }
  }

  async presentEmailPrompt(): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      const alert = await this.alertCtrl.create({
        header: 'Empfänger eingeben',
        inputs: [
          {
            name: 'emailAddress',
            type: 'email',
            placeholder: 'Email Adresse',
          },
        ],
        buttons: [
          {
            text: 'Abbrechen',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Cancel');
              reject();
            },
          },
          {
            text: 'Ok',
            handler: data => {
              console.log('Ok', data.emailAddress);
              resolve(data.emailAddress);
            },
          },
        ],
      });

      await alert.present();
    });
  }

  async dayClicked(day: number) {
    console.log('dayClicked', day);
    this.router.navigate([
      'list-choice',
      { year: this.calendar.year, month: this.calendar.month + 1, day },
    ]);
  }

  async generateMocks() {
    await this.mockService.generateRandomMocks(100);
    this.updated = new Date();
  }
}
