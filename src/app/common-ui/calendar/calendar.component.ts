import { Component, ContentChild, OnInit, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-ui-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  year: number;
  month: number;
  date: Date;
  days: number[];
  today: number;

  @ContentChild('content') contentTemplate: TemplateRef<any>;

  updateSubject = new Subject();
  clickSubject = new Subject<number>();

  constructor() {}

  ngOnInit() {
    this.date = new Date();
    this.year = this.date.getFullYear();
    this.month = this.date.getMonth();
    this.calcDays();
  }

  calcDays() {
    this.date = new Date(this.year, this.month);
    const numDays = this.daysInMonth(this.year, this.month);
    this.days = Array(numDays)
      .fill(0)
      .map((x, i) => i + 1);
    const dateToday = new Date();
    if (
      dateToday.getFullYear() === this.year &&
      dateToday.getMonth() === this.month
    )
      this.today = dateToday.getDate();
    else this.today = -1;
  }

  prev() {
    this.month--;
    if (this.month < 0) {
      this.month = 11;
      this.year--;
    }
    this.calcDays();
    this.updateSubject.next();
  }

  next() {
    this.month++;
    if (this.month > 11) {
      this.month = 0;
      this.year++;
    }
    this.calcDays();
    this.updateSubject.next();
  }

  dayClicked(day: number) {
    this.clickSubject.next(day);
  }

  private daysInMonth(year, monthIndex): number {
    return new Date(year, monthIndex + 1, 0).getDate(); // next month, but one day prior
  }
}
